#All scopes are country scope
#All parameters are optional

estate_boar_command_land_rights = {
	icon = privilege_grant_autonomy
	loyalty = 0.05
	influence = 0.05
	land_share = 5
	max_absolutism = -5
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
		governing_capacity = 100
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_boar_command_military_thinking = {
	icon = privilege_military_power
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
		country_military_power = 1
	}
	
	ai_will_do = {
		#factor = 
	}
}

estate_boar_command_x = {
	#icon = 
	loyalty = 0.1
	influence = 0.1
	land_share = 0
	max_absolutism = -10
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_boar_command_iron_drills = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
		fire_damage_received = -0.1
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_boar_command_exaltations_from_valour = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
		fire_damage_received = -0.1
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_boar_command_management = {	# this need more details
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	on_granted = {
		if = {
			limit = {
				any_owned_province = {
					has_province_modifier = hob_boar_war_camp # make this check teh correct province/modifier
				}
			}
			custom_tooltip = "Add a cool modifier in the Boar War Camp"
		}
		else = {
			custom_tooltip = "Has no effects upon being granted."
		}
	}
	
	on_granted_province = {
		random_owned_province = {
			limit = { has_province_modifier = hob_boar_war_camp } # make this check teh correct province/modifier
			custom_tooltip = estate_boar_command_management_tt
			hidden_effect = { add_province_triggered_modifier = hob_boar_management }
		}
	}
	
	on_revoked = {
		custom_tooltip = "Remove the cool modifier in the Boar War Camp"
	}
	
	on_revoked_province = {
		random_owned_province = {
			limit = { has_province_modifier = hob_boar_management }
			custom_tooltip = revoke_estate_boar_command_management_tt
			hidden_effect = { remove_province_triggered_modifier = hob_boar_management }
		}
	}
	
	on_invalid = {
		custom_tooltip = "Remove the cool modifier in the Boar War Camp"
	}
	
	on_invalid_province = {
		random_owned_province = {
			limit = { has_province_modifier = hob_boar_management }
			custom_tooltip = revoke_estate_boar_command_management_tt
			hidden_effect = { remove_province_triggered_modifier = hob_boar_management }
		}
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	modifier_by_land_ownership = {
		global_manpower_modifier = 0.25
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_boar_command_ninyu_kikun_nosunin = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	is_valid = {
	}
	
	on_granted = {
		custom_tooltip = estate_boar_command_ninyu_kikun_nosunin_tt
		capital_scope = {
			add_unit_construction = {
				type = infantry
				amount = 5
				speed = 1
				cost = 0
			}
		}
	}
	
	penalties = {
	}
	
	benefits = {
		infantry_shock = 0.1
	}
	
	conditional_modifier = {
		trigger = { faction_in_power = hob_boar_command }
		modifier = {
			infantry_cost = -0.1
		}
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}